import React from 'react';
import {useCharacters} from '../hooks/useCharacters';
import './characters-list.css';
import {Link} from 'react-router-dom';

export default function CharactersLst() {
  const {data, error, loading} = useCharacters();
  
  if (loading) {return <div>loading...</div>}
  if (error) {return <div>something went wrong</div>}
  return (
    <div className='characters'>
      {data.characters.results.map(character => {
        return <Link to={`/${character.id}`} key={character.id}> 

          <img src={character.image}/>
          <h2>{character.name}</h2>
        </Link>
      })}

    </div>
  )
}