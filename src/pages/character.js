import React from 'react';
import {useCharacter} from '../hooks/useCharacter';
import './character.css';
import {useParams} from 'react-router-dom';
import {Link} from 'react-router-dom';


export default function Character () {

  const {id} = useParams()
  const {data, loading, error} = useCharacter(id);
  console.log({error, loading, data})

  if (loading) {return <div>loading...</div>}
  if (error) {return <div>something went wrong</div>}
  return (
    <div className='character'>
      <div className='character-area'>
        <img className='image-character' src={data.character.image}/>
        <Link to='/'>
          Back
        </Link>
      </div>
      <div>
        <div>
          {data.character.name}
        </div>
        <div>
          {data.character.gender}
        </div>
        {data.character.episode.map(character => {
          return <div key={character.id}>
              {character.name} - {character.episode}
             </div>
        })}


      </div>

    </div>
  )
}