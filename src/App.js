import React from 'react';
import {Routes, Route} from 'react-router-dom';
import './App.css';
import CharactersLst from './pages/characters-list';
import Character from './pages/character';
import Search from './pages/search';

function App() {
  return (
    <div className="App">
      <Routes>
        <Route path="/" 
          element={<CharactersLst/>}/>
        <Route path='/search'
          element={<Search/>}/>
        <Route path='/:id'
          element={<Character/>}/>
      </Routes>
  
    </div>
  );
}

export default App;
